//GET ID:69
fetch("https://jsonplaceholder.typicode.com/todos/69")
    .then((res) => res.json())
    .then((data) => console.table(data));

//GET TODOS
fetch("https://jsonplaceholder.typicode.com/todos")
    .then((res) => res.json())
    .then((data) => {
        let list = data.map((d) => d.title);
        console.table(list);
    });

//POST
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ title: "Created To Do List Item", completed: false, userId: 1 }),
})
    .then((res) => res.json())
    .then((data) => console.table(data));

//PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
        title: "Updated To Do List Item",
        description: "To update the my to do list with a different data structure.",
        status: "Pending",
        dateCompleted: "Pending",
        userId: 1,
    }),
})
    .then((res) => res.json())
    .then((data) => console.table(data));

//PATCH
fetch("https://jsonplaceholder.typicode.com/todos/79", {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ status: "Complete", dateCompleted: "01/19/22" }),
})
    .then((res) => res.json())
    .then((data) => console.table(data));

//DELETE
fetch("https://jsonplaceholder.typicode.com/todos/69", {
    method: "DELETE",
})
    .then((res) => res.json())
    .then((data) => console.table(data));
